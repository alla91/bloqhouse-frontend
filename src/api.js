import axios from 'axios'

const hostname = 'http://localhost:3000'

const api = {
  getGames: async (onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'GET',
        url: `${hostname}/games`
      })
      return onSuccess(response.data)
    } catch (error) {
      return onError(error)
    }
  }

}

export default api
