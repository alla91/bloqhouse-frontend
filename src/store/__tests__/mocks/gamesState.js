const state = {
  list: [
    {
      id: '1',
      name: 'Test name 1',
      summary: 'Test summary 1',
      cover: {
        url: 'testUrl1'
      }
    },
    {
      id: '2',
      name: 'Test name 2',
      summary: 'Test summary 2',
      cover: {
        url: 'testUrl2'
      }
    },
    {
      id: '3',
      name: 'Test name 3',
      summary: 'Test summary 3',
      cover: {
        url: 'testUrl3'
      }
    }
  ],
  isLoading: false
}

export default state
