import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import games from '../../modules/games'
import gamesStateMock from '../mocks/gamesState'
import * as types from '../../mutation-types'
const hostname = 'http://localhost:3000'

describe('Games', () => {
  let API = new MockAdapter(axios)

  describe('State', () => {
    it('should contain the same keys as the mock', () => {
      let keysInModule = Object.keys(games.state)
      let keysInMock = Object.keys(gamesStateMock)
      expect(keysInModule).toEqual(keysInMock)
    })
  })

  describe('Getters', () => {
    let state
    beforeEach(() => {
      state = {...gamesStateMock}
    })

    it('returns the games list on getGamesList', () => {
      const getterResult = games.getters.getGamesList(state)
      expect(getterResult).toEqual(gamesStateMock.list)
    })

    it('returns isLoading on getIsLoading', () => {
      const getterResult = games.getters.getIsLoading(state)
      expect(getterResult).toEqual(gamesStateMock.isLoading)
    })
  })

  describe('Actions', () => {
    let commit = {
      count: 0,
      payload: ''
    }

    beforeEach(() => {
      commit.count = 0
      commit.payload = ''
    })

    afterEach(() => {
      API.reset()
    })

    describe('getGames', () => {
      let path = `${hostname}/games`
      describe('onSuccess', () => {
        let mockCommit = (type) => {
          if (type === types.RECEIVE_GAMES || type === types.TOGGLE_IS_LOADING) {
            commit.count += 1
          }
        }

        it('commits RECEIVE_GAMES and TOGGLE_IS_LOADING', async () => {
          API.onGet(path).reply(200, gamesStateMock.list)
          await games.actions.getGames({commit: mockCommit})
          expect(commit.count).toEqual(3)
        })
      })
      describe('onError', () => {
        let mockCommit = () => {
          commit.count += 1
        }

        it('commits TOGGLE_IS_LOADING', (done) => {
          API.onGet(path).reply(404, 'Not found')
          games.actions.getGames({commit: mockCommit})
            .catch(() => {
              expect(commit.count).toEqual(1)
              done()
            })
        })
      })
    })
  })

  describe('Mutations', () => {
    it('RECEIVE_GAMES mutation sets the game list correctly', () => {
      games.mutations[types.RECEIVE_GAMES](games.state, {games: gamesStateMock.list})
      expect(games.state.list).toEqual(gamesStateMock.list)
    })

    it('TOGGLE_IS_LOADING mutation sets the isLoading boolean correctly', () => {
      games.mutations[types.TOGGLE_IS_LOADING](games.state, {isLoading: gamesStateMock.isLoading})
      expect(games.state.isLoading).toEqual(gamesStateMock.isLoading)
    })
  })
})
