import api from '../../api'
import * as types from '../mutation-types'

const namespaced = true

const state = {
  list: [],
  isLoading: false
}

const getters = {
  getGamesList: state => state.list,
  getIsLoading: state => state.isLoading
}

const actions = {
  async getGames ({commit}) {
    commit(types.TOGGLE_IS_LOADING, {isLoading: true})
    await api.getGames(
      (response) => {
        commit(types.RECEIVE_GAMES, {games: response})
        commit(types.TOGGLE_IS_LOADING, {isLoading: false})
        return Promise.resolve()
      },
      (error) => {
        return Promise.reject(error)
      }
    )
  }
}

const mutations = {
  [types.RECEIVE_GAMES] (state, {games}) {
    Object.assign(state.list, games)
  },
  [types.TOGGLE_IS_LOADING] (state, {isLoading}) {
    state.isLoading = isLoading
  }
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
