import { shallowMount, createLocalVue } from '@vue/test-utils'
import GamesGrid from '../GamesGrid'
import Vuex from 'vuex'

describe('GamesGrid', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuex)
  })

  describe('When header is mounted', () => {
    it('has the expected html structure when it\'s loading', () => {
      store = new Vuex.Store({
        getters: {
          'games/getGamesList': () => [],
          'games/getIsLoading': () => true
        },
        actions: {
          'games/getGames': () => []
        }
      })
      wrapper = shallowMount(GamesGrid, { store, localVue })
      expect(wrapper.element).toMatchSnapshot()
    })

    it('has the expected html structure when it finished loading', () => {
      store = new Vuex.Store({
        getters: {
          'games/getGamesList': () => [],
          'games/getIsLoading': () => false
        },
        actions: {
          'games/getGames': () => []
        }
      })
      wrapper = shallowMount(GamesGrid, { store, localVue })
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
