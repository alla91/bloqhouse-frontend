import { shallowMount, createLocalVue } from '@vue/test-utils'
import PageHeader from '../PageHeader'

describe('PageHeader', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    wrapper = shallowMount(PageHeader, { localVue })
  })

  describe('When header is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
