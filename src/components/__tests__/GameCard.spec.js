import { shallowMount, createLocalVue } from '@vue/test-utils'
import GameCard from '../GameCard'
import Vuex from 'vuex'

describe('GameCard', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuex)
    store = new Vuex.Store({})
    wrapper = shallowMount(GameCard, {
      store,
      localVue,
      propsData: {
        game: {
          id: 1,
          name: 'test name',
          cover: {
            url: 'test url'
          },
          summary: 'test summary'
        }
      }
    })
  })

  describe('When header is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
