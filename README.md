# Running the project

1. `cd` to the **/bloqhouse-frontend** directory
2. `yarn` or `npm install` for downloading node_modules
3. `yarn serve` or `npm run serve`

# Running the tests
1. `cd` to the **/bloqhouse-frontend** directory
2. `yarn test:unit` or `npm run test:unit`

# Enjoy!